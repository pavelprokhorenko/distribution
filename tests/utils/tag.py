from fastapi.testclient import TestClient

TAG_NAME = "name"


def create_tag(api_client: TestClient, tag_name):
    payload = dict(name=tag_name)
    return api_client.post("v1/tag/", json=payload)


def get_tag(api_client: TestClient, tag_id):
    return api_client.get(f"v1/tag/{tag_id}")


def delete_tag(api_client: TestClient, tag_id):
    return api_client.delete(f"v1/tag/{tag_id}")
