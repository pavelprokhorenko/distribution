from datetime import datetime

from fastapi.encoders import jsonable_encoder
from fastapi.testclient import TestClient

MESSAGE_DELIVERED = jsonable_encoder(datetime.now())
MESSAGE_STATE = "failed"

MESSAGE_STATE_UPDATED = "success"


def create_message(api_client: TestClient, message_data):
    return api_client.post("v1/message/", json=message_data)


def get_message(api_client: TestClient, message_id):
    return api_client.get(f"v1/message/{message_id}")


def update_message(api_client: TestClient, message_data):
    return api_client.patch("v1/message/", json=message_data)


def delete_message(api_client: TestClient, message_id):
    return api_client.delete(f"v1/message/{message_id}")
