import pytest
import responses
from fastapi import status
from fastapi.testclient import TestClient

from tests.utils import (
    CLIENT_MOBILE_CODE_OPERATOR,
    CLIENT_PHONE_NUMBER,
    DISTRIBUTION_CLIENT_MOBILE_OPERATOR_CODE,
    DISTRIBUTION_MESSAGE_TEXT,
    DISTRIBUTION_START,
    DISTRIBUTION_STOP,
    TAG_NAME,
    create_client,
    create_distribution,
    create_tag,
    get_distribution,
    get_message,
    send_distributions,
)

pytestmark = pytest.mark.usefixtures("use_postgres")

CLIENTS_DATA = [
    dict(
        phone_number=CLIENT_PHONE_NUMBER,
        mobile_operator_code=CLIENT_MOBILE_CODE_OPERATOR,
        tags=[TAG_NAME],
    ),
    dict(
        phone_number="+75698722345",
        mobile_operator_code=CLIENT_MOBILE_CODE_OPERATOR,
        tags=[TAG_NAME],
    ),
    dict(phone_number="+75698722345", mobile_operator_code=1564654, tags=[TAG_NAME]),
    dict(
        phone_number="+75697722345",
        mobile_operator_code=CLIENT_MOBILE_CODE_OPERATOR,
        tags=[TAG_NAME + "1"],
    ),
]
DISTRIBUTION_DATA = dict(
    start=DISTRIBUTION_START,
    stop=DISTRIBUTION_STOP,
    message_text=DISTRIBUTION_MESSAGE_TEXT,
    client_mobile_operator_code=DISTRIBUTION_CLIENT_MOBILE_OPERATOR_CODE,
    tags=[TAG_NAME],
)


class TestSendDistributions:
    @responses.activate
    def test_ok(self, api_client: TestClient) -> None:
        responses.add(
            responses.POST,
            "https://probe.fbrq.cloud/v1/send/1",
            status=200,
        )

        create_tag(api_client=api_client, tag_name=TAG_NAME)
        create_tag(api_client=api_client, tag_name=TAG_NAME + "1")
        for client in CLIENTS_DATA:
            create_client(api_client=api_client, client_data=client)

        distribution = create_distribution(
            api_client=api_client, distribution_data=DISTRIBUTION_DATA
        )
        distribution_id = distribution.json().get("id")

        response = send_distributions(
            api_client=api_client, auth_token="test-token", start=DISTRIBUTION_START
        )

        assert response.status_code == status.HTTP_204_NO_CONTENT
        distribution = get_distribution(
            api_client=api_client, distribution_id=distribution_id
        )
        messages = distribution.json().get("messages")
        assert len(messages) == 2

        for message_id in messages:
            msg = get_message(api_client=api_client, message_id=message_id)
            assert msg.json().get("state") == "Success"

    def test_invalid_token(self, api_client: TestClient) -> None:
        create_tag(api_client=api_client, tag_name=TAG_NAME)
        create_tag(api_client=api_client, tag_name=TAG_NAME + "1")
        for client in CLIENTS_DATA:
            create_client(api_client=api_client, client_data=client)

        distribution = create_distribution(
            api_client=api_client, distribution_data=DISTRIBUTION_DATA
        )
        distribution_id = distribution.json().get("id")

        response = send_distributions(
            api_client=api_client,
            auth_token="invalid-test-token",
            start=DISTRIBUTION_START,
        )

        assert response.status_code == status.HTTP_204_NO_CONTENT
        distribution = get_distribution(
            api_client=api_client, distribution_id=distribution_id
        )
        messages = distribution.json().get("messages")
        assert len(messages) == 2

        for message_id in messages:
            msg = get_message(api_client=api_client, message_id=message_id)
            assert msg.json().get("state") == "Failed"
