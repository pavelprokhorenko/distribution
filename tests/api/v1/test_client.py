import pytest
from fastapi import status
from fastapi.testclient import TestClient

from tests.utils import (
    CLIENT_MOBILE_CODE_OPERATOR,
    CLIENT_MOBILE_CODE_OPERATOR_UPDATED,
    CLIENT_PHONE_NUMBER,
    CLIENT_PHONE_NUMBER_UPDATED,
    TAG_NAME,
    create_client,
    create_tag,
    delete_client,
    get_client,
    update_client,
)

pytestmark = pytest.mark.usefixtures("use_postgres")

CLIENT_DATA = dict(
    phone_number=CLIENT_PHONE_NUMBER,
    mobile_operator_code=CLIENT_MOBILE_CODE_OPERATOR,
    tags=[TAG_NAME],
)
UPDATED_CLIENT_DATA = dict(
    phone_number=CLIENT_PHONE_NUMBER_UPDATED,
    mobile_operator_code=CLIENT_MOBILE_CODE_OPERATOR_UPDATED,
    tags=[TAG_NAME],
)


class TestCreateClient:
    def test_ok(self, api_client: TestClient) -> None:
        create_tag(api_client=api_client, tag_name=TAG_NAME)

        response = create_client(api_client=api_client, client_data=CLIENT_DATA)

        assert response.status_code == status.HTTP_201_CREATED
        data = response.json()
        assert isinstance(data.get("id"), int)
        assert data.get("phone_number") == CLIENT_PHONE_NUMBER
        assert data.get("mobile_operator_code") == CLIENT_MOBILE_CODE_OPERATOR
        assert data.get("time_zone") == "UTC"
        assert TAG_NAME in data.get("tags")

    def test_invalid_phone_number(self, api_client: TestClient) -> None:
        create_tag(api_client=api_client, tag_name=TAG_NAME)
        client_data = CLIENT_DATA.copy()
        client_data["phone_number"] = "456"
        response = create_client(api_client=api_client, client_data=client_data)

        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
        assert response.json() is not None


class TestGetClient:
    def test_ok(self, api_client: TestClient) -> None:
        create_tag(api_client=api_client, tag_name=TAG_NAME)

        client = create_client(api_client=api_client, client_data=CLIENT_DATA)
        client_id = client.json().get("id")
        response = get_client(api_client=api_client, client_id=client_id)

        assert response.status_code == status.HTTP_200_OK
        data = response.json()
        assert isinstance(data.get("id"), int)
        assert data.get("phone_number") == CLIENT_PHONE_NUMBER
        assert data.get("mobile_operator_code") == CLIENT_MOBILE_CODE_OPERATOR
        assert data.get("time_zone") == "UTC"
        assert TAG_NAME in data.get("tags")

    def test_invalid_id(self, api_client: TestClient) -> None:
        create_tag(api_client=api_client, tag_name=TAG_NAME)

        create_client(api_client=api_client, client_data=CLIENT_DATA)

        response = get_client(api_client=api_client, client_id=-1)

        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
        assert response.json() is not None


class TestUpdateClient:
    def test_ok(self, api_client: TestClient) -> None:
        create_tag(api_client=api_client, tag_name=TAG_NAME)

        client = create_client(api_client=api_client, client_data=CLIENT_DATA)
        client_data = client.json()

        client_data_updated = UPDATED_CLIENT_DATA.copy()
        client_data_updated["id"] = client_data.get("id")

        response = update_client(api_client=api_client, client_data=client_data_updated)

        assert response.status_code == status.HTTP_202_ACCEPTED
        updated_client_data = response.json()
        assert isinstance(updated_client_data.get("id"), int)
        assert updated_client_data.get("phone_number") == CLIENT_PHONE_NUMBER_UPDATED
        assert (
            updated_client_data.get("mobile_operator_code")
            == CLIENT_MOBILE_CODE_OPERATOR_UPDATED
        )
        assert updated_client_data.get("time_zone") == "UTC"
        assert TAG_NAME in updated_client_data.get("tags")

        assert updated_client_data.get("phone_number") != client_data.get(
            "phone_number"
        )
        assert updated_client_data.get("mobile_operator_code") != client_data.get(
            "mobile_operator_code"
        )
        assert updated_client_data.get("time_zone") == client_data.get("time_zone")
        assert updated_client_data.get("tags") == client_data.get("tags")

    def test_invalid_data(self, api_client: TestClient) -> None:
        create_tag(api_client=api_client, tag_name=TAG_NAME)

        client = create_client(api_client=api_client, client_data=CLIENT_DATA)
        client_data = client.json()

        client_data_updated = UPDATED_CLIENT_DATA.copy()
        client_data_updated["id"] = client_data.get("id")
        client_data_updated["phone_number"] = "4654"

        response = update_client(api_client=api_client, client_data=client_data_updated)

        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
        assert response.json() is not None


class TestDeleteClient:
    def test_ok(self, api_client: TestClient) -> None:
        create_tag(api_client=api_client, tag_name=TAG_NAME)

        client = create_client(api_client=api_client, client_data=CLIENT_DATA)
        client_data = client.json()

        response = delete_client(api_client=api_client, client_id=client_data.get("id"))

        assert response.status_code == status.HTTP_204_NO_CONTENT
        assert response.json() is None

        client = get_client(api_client=api_client, client_id=client_data.get("id"))

        assert client.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
        assert client.json() is not None

    def test_invalid_id(self, api_client: TestClient) -> None:
        create_tag(api_client=api_client, tag_name=TAG_NAME)

        client = create_client(api_client=api_client, client_data=CLIENT_DATA)
        client_data = client.json()

        response = delete_client(api_client=api_client, client_id=-1)

        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
        assert response.json() is not None

        client = get_client(api_client=api_client, client_id=client_data.get("id"))

        assert client.status_code == status.HTTP_200_OK
        assert client.json() is not None
