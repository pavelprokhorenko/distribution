import pytest
from fastapi import status
from fastapi.testclient import TestClient

from tests.utils import (
    DISTRIBUTION_CLIENT_MOBILE_OPERATOR_CODE,
    DISTRIBUTION_MESSAGE_TEXT,
    DISTRIBUTION_MESSAGE_TEXT_UPDATED,
    DISTRIBUTION_START,
    DISTRIBUTION_STOP,
    TAG_NAME,
    create_distribution,
    create_tag,
    delete_distribution,
    get_distribution,
    update_distribution,
)

pytestmark = pytest.mark.usefixtures("use_postgres")

DISTRIBUTION_DATA = dict(
    start=DISTRIBUTION_START,
    stop=DISTRIBUTION_STOP,
    message_text=DISTRIBUTION_MESSAGE_TEXT,
    client_mobile_operator_code=DISTRIBUTION_CLIENT_MOBILE_OPERATOR_CODE,
    tags=[TAG_NAME],
)
DISTRIBUTION_DATA_UPDATED = DISTRIBUTION_DATA.copy()
DISTRIBUTION_DATA_UPDATED["message_text"] = DISTRIBUTION_MESSAGE_TEXT_UPDATED
DISTRIBUTION_DATA_UPDATED["tags"] = []


class TestCreateDistribution:
    def test_ok(self, api_client: TestClient) -> None:
        create_tag(api_client=api_client, tag_name=TAG_NAME)

        response = create_distribution(
            api_client=api_client, distribution_data=DISTRIBUTION_DATA
        )

        assert response.status_code == status.HTTP_201_CREATED
        data = response.json()
        assert isinstance(data.get("id"), int)
        assert data.get("start") == DISTRIBUTION_START
        assert data.get("stop") == DISTRIBUTION_STOP
        assert data.get("message_text") == DISTRIBUTION_MESSAGE_TEXT
        assert (
            data.get("client_mobile_operator_code")
            == DISTRIBUTION_CLIENT_MOBILE_OPERATOR_CODE
        )
        assert TAG_NAME in data.get("tags")

    def test_invalid_start(self, api_client: TestClient) -> None:
        create_tag(api_client=api_client, tag_name=TAG_NAME)
        distribution_data = DISTRIBUTION_DATA.copy()
        distribution_data["start"] = "7:00 PM"

        response = create_distribution(
            api_client=api_client, distribution_data=distribution_data
        )

        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
        assert response.json() is not None


class TestGetDistribution:
    def test_ok(self, api_client: TestClient) -> None:
        create_tag(api_client=api_client, tag_name=TAG_NAME)

        distribution = create_distribution(
            api_client=api_client, distribution_data=DISTRIBUTION_DATA
        )
        distribution_id = distribution.json().get("id")

        response = get_distribution(
            api_client=api_client, distribution_id=distribution_id
        )

        assert response.status_code == status.HTTP_200_OK
        data = response.json()
        assert isinstance(data.get("id"), int)
        assert data.get("start") == DISTRIBUTION_START
        assert data.get("stop") == DISTRIBUTION_STOP
        assert data.get("message_text") == DISTRIBUTION_MESSAGE_TEXT
        assert (
            data.get("client_mobile_operator_code")
            == DISTRIBUTION_CLIENT_MOBILE_OPERATOR_CODE
        )
        assert TAG_NAME in data.get("tags")

    def test_invalid_id(self, api_client: TestClient) -> None:
        create_tag(api_client=api_client, tag_name=TAG_NAME)

        create_distribution(api_client=api_client, distribution_data=DISTRIBUTION_DATA)

        response = get_distribution(api_client=api_client, distribution_id=-1)

        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
        assert response.json() is not None


class TestUpdateDistribution:
    def test_ok(self, api_client: TestClient) -> None:
        create_tag(api_client=api_client, tag_name=TAG_NAME)

        distribution = create_distribution(
            api_client=api_client, distribution_data=DISTRIBUTION_DATA
        )

        distribution_data = DISTRIBUTION_DATA_UPDATED.copy()
        distribution_data["id"] = distribution.json().get("id")

        response = update_distribution(
            api_client=api_client, distribution_data=distribution_data
        )

        assert response.status_code == status.HTTP_202_ACCEPTED
        data = response.json()
        assert isinstance(data.get("id"), int)
        assert data.get("start") == DISTRIBUTION_START
        assert data.get("stop") == DISTRIBUTION_STOP
        assert data.get("message_text") == DISTRIBUTION_MESSAGE_TEXT_UPDATED
        assert (
            data.get("client_mobile_operator_code")
            == DISTRIBUTION_CLIENT_MOBILE_OPERATOR_CODE
        )
        assert TAG_NAME not in data.get("tags")

    def test_invalid_data(self, api_client: TestClient) -> None:
        create_tag(api_client=api_client, tag_name=TAG_NAME)

        distribution = create_distribution(
            api_client=api_client, distribution_data=DISTRIBUTION_DATA
        )

        distribution_data = DISTRIBUTION_DATA_UPDATED.copy()
        distribution_data["id"] = distribution.json().get("id")
        distribution_data["start"] = "15:52"

        response = update_distribution(
            api_client=api_client, distribution_data=distribution_data
        )

        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
        assert response.json() is not None


class TestDeleteDistribution:
    def test_ok(self, api_client: TestClient) -> None:
        create_tag(api_client=api_client, tag_name=TAG_NAME)

        distribution = create_distribution(
            api_client=api_client, distribution_data=DISTRIBUTION_DATA
        )

        distribution_id = distribution.json().get("id")

        response = delete_distribution(
            api_client=api_client, distribution_id=distribution_id
        )

        assert response.status_code == status.HTTP_204_NO_CONTENT
        assert response.json() is None

        distribution = get_distribution(
            api_client=api_client, distribution_id=distribution_id
        )

        assert distribution.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
        assert distribution.json() is not None

    def test_invalid_id(self, api_client: TestClient) -> None:
        create_tag(api_client=api_client, tag_name=TAG_NAME)

        distribution = create_distribution(
            api_client=api_client, distribution_data=DISTRIBUTION_DATA
        )

        distribution_id = distribution.json().get("id")

        response = delete_distribution(api_client=api_client, distribution_id=-1)

        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
        assert response.json() is not None

        distribution = get_distribution(
            api_client=api_client, distribution_id=distribution_id
        )

        assert distribution.status_code == status.HTTP_200_OK
        assert distribution.json() is not None
