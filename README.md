# Web

## Dependencies

* [Docker](https://www.docker.com/).
* [Docker Compose](https://docs.docker.com/compose/install/).
* [Poetry](https://python-poetry.org/) for Python package and environment management.

## Development

* Run server with all dependencies:

```console
$ docker-compose up --build
```
* Add pre-commit hook

```console
$ pre-commit install
```

## Tests

Testing is done with pytest, tests are placed in the "tests" directory

For testing, you should go inside the container and run the command `pytest .`

```console
$ docker exec -it <FastAPI container ID> bash
$ pytest .
```
Or you can see full information about running tests:
```console
$ docker exec -it <FastAPI container ID> bash
$ pytest -vvv -s
```

## Project structure

```
-> *alembic* — migrations folder
-> *app*
   -> *api* — layer with logic for processing requests via api
   -> *core* — global things for the project, like settings (`config.py`)
    -> *db* — database and session initialization
   -> *models* — models in SQLAlchemy terminology (not to be confused with *schemas* in pydantic and business models)
   -> *schemas* — schemes for validating/serializing request/response objects (they are also models in pydantic terminology)
   -> *services* — service layer, all business logic is placed here.
   -> *utils* — utility logic.
-> *tests* — root for tests
-> *.env.template* — file to list all environment variables used inside the service
```

## Migrations

* After creating the model in `app/models`, you need to import it in `app/db/base.py` (in order to make it visible to alembic)
* Make migrations (run inside container)

```console
$ alembic -n postgres revision --autogenerate -m "add column last_name to User model"
```

* Run migrations

```console
$ alembic -n postgres upgrade head
```

* Postgres
```
$ alembic -n postgres revision --autogenerate -m "text"
$ alembic -n postgres upgrade head
$ alembic -n postgres downgrade -1
```

## Cron
* Run
```
$ python3 app/cron/cron.py
```

## API
* API Swagger docs - "\<API host\>/docs"

## Выполненные дополнительные пункты
* 1. организовать тестирование написанного кода
* 3. подготовить docker-compose для запуска всех сервисов проекта одной командой
* 5. сделать так, чтобы по адресу /docs/ открывалась страница со Swagger UI и в нём отображалось описание разработанного API.
* 8. реализовать дополнительный сервис, который раз в сутки отправляет статистику по обработанным рассылкам на email
* 9. удаленный сервис может быть недоступен, долго отвечать на запросы или выдавать некорректные ответы. Необходимо организовать обработку ошибок и откладывание запросов при неуспехе для последующей повторной отправки. Задержки в работе внешнего сервиса никак не должны оказывать влияние на работу сервиса рассылок.
* 10. реализовать отдачу метрик в формате prometheus и задокументировать эндпоинты и экспортируемые метрики (наполовину)
* 11. реализовать дополнительную бизнес-логику: добавить в сущность "рассылка" поле "временной интервал", в котором можно задать промежуток времени, в котором клиентам можно отправлять сообщения с учётом их локального времени. Не отправлять клиенту сообщение, если его локальное время не входит в указанный интервал.
