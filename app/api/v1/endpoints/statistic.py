from typing import Dict, Optional

from fastapi import APIRouter, Depends, Path, Query, status
from sqlalchemy.orm import Session

from app import services
from app.api.deps import get_db_pg

router = APIRouter()


@router.get(
    "/distributions",
    response_model=Dict,
    status_code=status.HTTP_200_OK,
    summary='Retrieve all distribution statistic for "n" days',
)
async def get_distributions_stat(
    days: Optional[int] = Query(1),
    page: Optional[int] = Query(1),
    unit_per_page: Optional[int] = Query(12),
    db: Session = Depends(get_db_pg),
) -> Dict:
    return services.get_distributions_stat(
        db=db, days=days, page=page, unit_per_page=unit_per_page
    )


@router.get(
    "/messages/{distribution_id}",
    response_model=Dict,
    status_code=status.HTTP_200_OK,
    summary="Retrieve all message statistic for distribution",
)
async def get_messages_stat(
    distribution_id: int = Path(...),
    page: Optional[int] = Query(1),
    unit_per_page: Optional[int] = Query(12),
    db: Session = Depends(get_db_pg),
) -> Dict:
    return services.get_messages_stat(
        db=db, distribution_id=distribution_id, page=page, unit_per_page=unit_per_page
    )
