from fastapi import APIRouter, Body, Depends, Path, status
from sqlalchemy.orm import Session

from app import schemas, services
from app.api.deps import get_db_pg

router = APIRouter()


@router.get(
    "/{tag_id}",
    response_model=schemas.TagOut,
    status_code=status.HTTP_200_OK,
    summary="Retrieve tag by id",
)
async def get_tag(
    tag_id: int = Path(...), db: Session = Depends(get_db_pg)
) -> schemas.TagOut:
    return services.get_tag(db=db, tag_id=tag_id)


@router.post(
    "/",
    response_model=schemas.TagOut,
    status_code=status.HTTP_201_CREATED,
    summary="Create tag",
)
async def create_tag(
    tag: schemas.TagIn = Body(...), db: Session = Depends(get_db_pg)
) -> schemas.TagOut:
    return services.create_tag(db=db, tag=tag)


@router.delete(
    "/{tag_id}",
    status_code=status.HTTP_204_NO_CONTENT,
    summary="Delete tag by id",
)
async def delete_tag(tag_id: int = Path(...), db: Session = Depends(get_db_pg)) -> None:
    return services.delete_tag(db=db, tag_id=tag_id)
