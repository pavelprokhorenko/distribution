from datetime import datetime

from fastapi import APIRouter, Body, Depends, Path, status
from sqlalchemy.orm import Session

from app import schemas, services
from app.api.deps import get_db_pg

router = APIRouter()


@router.get(
    "/{distribution_id}",
    response_model=schemas.DistributionOut,
    status_code=status.HTTP_200_OK,
    summary="Retrieve distribution by id",
)
async def get_distribution(
    distribution_id: int = Path(...), db: Session = Depends(get_db_pg)
) -> schemas.DistributionOut:
    return services.get_distribution(db=db, distribution_id=distribution_id)


@router.post(
    "/",
    response_model=schemas.DistributionOut,
    status_code=status.HTTP_201_CREATED,
    summary="Create distribution",
)
async def create_distribution(
    distribution: schemas.DistributionIn = Body(...), db: Session = Depends(get_db_pg)
) -> schemas.DistributionOut:
    return services.create_distribution(db=db, distribution=distribution)


@router.put(
    "/",
    response_model=schemas.DistributionOut,
    status_code=status.HTTP_202_ACCEPTED,
    summary="Update distribution",
)
async def update_distribution(
    distribution: schemas.DistributionOut = Body(...), db: Session = Depends(get_db_pg)
) -> schemas.DistributionOut:
    return services.update_distribution(db=db, distribution=distribution)


@router.delete(
    "/{distribution_id}",
    status_code=status.HTTP_204_NO_CONTENT,
    summary="Delete distribution by id",
)
async def delete_distribution(
    distribution_id: int = Path(...), db: Session = Depends(get_db_pg)
) -> None:
    return services.delete_distribution(db=db, distribution_id=distribution_id)


@router.post(
    "/send",
    status_code=status.HTTP_204_NO_CONTENT,
    summary='Send distributions which are started at "start"',
)
async def send_distributions(
    auth_token: str = Body(...),
    start: datetime = Body(...),
    db: Session = Depends(get_db_pg),
) -> None:
    return services.send_distributions(db=db, auth_token=auth_token, start=start)
