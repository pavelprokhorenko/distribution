from fastapi import APIRouter, Body, Depends, Path, status
from sqlalchemy.orm import Session

from app import schemas, services
from app.api.deps import get_db_pg

router = APIRouter()


@router.get(
    "/{message_id}",
    response_model=schemas.MessageOut,
    status_code=status.HTTP_200_OK,
    summary="Retrieve message by id",
)
async def get_message(
    message_id: int = Path(...), db: Session = Depends(get_db_pg)
) -> schemas.MessageOut:
    return services.get_message(db=db, message_id=message_id)


@router.post(
    "/",
    response_model=schemas.MessageOut,
    status_code=status.HTTP_201_CREATED,
    summary="Create message",
)
async def create_message(
    message: schemas.MessageIn = Body(...), db: Session = Depends(get_db_pg)
) -> schemas.MessageOut:
    return services.create_message(db=db, message=message)


@router.patch(
    "/",
    response_model=schemas.MessageOut,
    status_code=status.HTTP_202_ACCEPTED,
    summary="Update message",
)
async def update_message(
    message: schemas.MessageInUpdate = Body(...), db: Session = Depends(get_db_pg)
) -> schemas.MessageOut:
    return services.update_message(db=db, message=message)


@router.delete(
    "/{message_id}",
    status_code=status.HTTP_204_NO_CONTENT,
    summary="Delete message by id",
)
async def delete_message(
    message_id: int = Path(...), db: Session = Depends(get_db_pg)
) -> None:
    return services.delete_message(db=db, message_id=message_id)
