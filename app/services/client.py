from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session

import app.db.base as models
from app import exceptions, schemas


def create_client(db: Session, client: schemas.ClientIn) -> schemas.ClientOut:
    """Create new client.
    Args:
        db:
            database session.
        client:
            new client.
    Raises:
        exceptions.ObjectAlreadyExist:
            client already exist.
    Returns:
        schemas.ClientOut:
            created client.
    """

    client_in_db = (
        db.query(models.Client)
        .filter(
            models.Client.phone_number == client.phone_number,
            models.Client.mobile_operator_code == client.mobile_operator_code,
            models.Client.time_zone == client.time_zone,
        )
        .first()
    )

    if client_in_db:
        raise exceptions.ObjectAlreadyExist("Client already exist")

    db_client = models.Client(**client.dict(exclude={"tags"}))
    tags = list(db.query(models.Tag).filter(models.Tag.name.in_(client.tags)))
    db_client.tags = tags
    models.save(db=db, data=db_client)

    return schemas.ClientOut(
        id=db_client.id,
        phone_number=db_client.phone_number,
        mobile_operator_code=db_client.mobile_operator_code,
        time_zone=db_client.time_zone,
        tags=[tag.name for tag in db_client.tags],
    )


def get_client(db: Session, client_id: int) -> schemas.ClientOut:
    """Retrieve client by id.
    Args:
        db:
            database session.
        client_id:
            client id.
    Raises:
        exceptions.ObjectDoesNotExist:
            Client already exist.
    Returns:
        schemas.ClientOut:
            client data.
    """
    db_client = db.query(models.Client).get(client_id)

    if not db_client:
        raise exceptions.ObjectDoesNotExist("Client does not exist")

    return schemas.ClientOut(
        id=db_client.id,
        phone_number=db_client.phone_number,
        mobile_operator_code=db_client.mobile_operator_code,
        time_zone=db_client.time_zone,
        tags=[tag.name for tag in db_client.tags],
    )


def update_client(db: Session, client: schemas.ClientOut) -> schemas.ClientOut:
    """Update client data.
    Args:
        db:
            database session.
        client:
            client data.
    Raises:
        IncorrectData:
            Incorrect data.
        exceptions.ObjectDoesNotExist:
            Client does not exist.
    Returns:
        schemas.ClientOut:
            updated client data.
    """
    db_client = db.query(models.Client).get(client.id)

    if not db_client:
        raise exceptions.ObjectDoesNotExist("Client does not exist")

    db_client.phone_number = client.phone_number
    db_client.mobile_operator_code = client.mobile_operator_code
    db_client.time_zone = client.time_zone or "UTC"
    tags = list(db.query(models.Tag).filter(models.Tag.name.in_(client.tags)))
    db_client.tags = tags

    try:
        models.save(db=db, data=db_client)
    except IntegrityError:
        raise exceptions.IncorrectData("Incorrect data")

    return schemas.ClientOut(
        id=db_client.id,
        phone_number=db_client.phone_number,
        mobile_operator_code=db_client.mobile_operator_code,
        time_zone=db_client.time_zone,
        tags=[tag.name for tag in db_client.tags],
    )


def delete_client(db: Session, client_id: int) -> None:
    """Remove client data.
    Args:
        db:
            database engine.
        client_id:
            client id.
    Raises:
        exceptions.ObjectDoesNotExist:
            Client with such id does not exist.
    Returns:
        None
    """
    db_client = db.query(models.Client).get(client_id)

    if not db_client:
        raise exceptions.ObjectDoesNotExist("Client with such id does not exist")

    db.delete(db_client)
    db.commit()
