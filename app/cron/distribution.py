import os
from datetime import datetime

import requests

from app.cron.cron import API_HOST

AUTH_TOKEN = os.getenv("AUTH_TOKEN")


def send_distributions():
    requests.post(
        f"{API_HOST}/v1/distribution/send",
        json=dict(
            auth_token=AUTH_TOKEN,
            start=str(datetime.now()),
        ),
    )
